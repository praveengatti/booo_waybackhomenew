﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Khadga.UtilScripts.LocalData;
using MoreMountains.CorgiEngine;
using Khadga.Localization;

public class SaveManager : MonoBehaviour
{

    private static SaveManager instance;
    public  int[] Levels;

        // Game Instance Singleton
    public static SaveManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
        //LocalizationManager.SetLanguages();
        //LocalizationManager.ActiveLanguage = LocalDataUtil.GetLocalDataOfType<Language>("ActiveLanguage");
         Levels = DataManager.instance.gameData.Levels;
        //for (int i = 0; i < 36; i++)
        //    Levels[i] = -1;
        //Levels[1] = 0;
        //Levels[11] = 0;
        //Levels[21] = 0;
        // DataManger.instance.gameData.Levels = Levels;
        //SaveData();
    }

    public void SaveData()
    {
        SavePlayerData();
    }

    public void SavePlayerData()
    {
        LocalDataUtil.SaveToLocal<List<AchivementManger.Achivement>>(AchivementManger.Instance.AchievementList, "Achievements");
        LocalDataUtil.SaveToLocal<List<AchivementManger.Achivement>>(AchivementManger.Instance.AchievementList2, "Achievements2");
        DataManager.instance.gameData.Levels = Levels;
        DataManager.instance.SaveData();
    }

    public void PurchaseInDemo()
    {
        if (MainMenu.Level_Index == -2)
        {
            DataManager.instance.gameData.IsChapterTwoUnclocked = true;
        }
        else if (MainMenu.Level_Index == -3)
        {
            DataManager.instance.gameData.IsChapterThreeUnlocked = true;
        }
        SaveData();
    }
    

    // Update is called once per frame
    void Update()
    { 
       
    }
}
