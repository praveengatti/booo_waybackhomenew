﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverDoorScript : MonoBehaviour
{
    public bool isTrigger;
    public GameObject door;
    int movementSpeed = 10;
    float timer = 0f;
    public bool IsCameraMovementNeeded;
    public float Duration;
    // Use this for initialization
    void Start()
    {
        isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isTrigger == true)
        {
            timer += Time.deltaTime;
            door.transform.Translate(Vector2.up * movementSpeed * Time.deltaTime);
        }
        if (timer >= 4f)
        {
            if(IsCameraMovementNeeded)
            this.gameObject.GetComponent<CameraMovementOnLever>().BackTracing = true;
             movementSpeed = 0;
            door.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (IsCameraMovementNeeded)
            {
                this.gameObject.GetComponent<CameraMovementOnLever>().Canvas.enabled = false;
                this.gameObject.GetComponent<CameraMovementOnLever>().Camera.SetActive(true);
                this.gameObject.GetComponent<CameraMovementOnLever>().MainCamera.SetActive(false);
                this.gameObject.GetComponent<CameraMovementOnLever>().Camera.gameObject.transform.position = this.GetComponent<CameraMovementOnLever>().MainCamera.transform.position;
                this.gameObject.GetComponent<CameraMovementOnLever>().IntialTransform = this.gameObject.GetComponent<CameraMovementOnLever>().Camera.transform.position;
                StartCoroutine(Wait(Duration));
                this.gameObject.GetComponent<CameraMovementOnLever>().CameraAction = true;
            }
            else
            {
                isTrigger = true;
            }

        }
    }

    IEnumerator Wait(float duration)
    {
        yield return new WaitForSeconds(duration);
    }
}
