﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.Localization;

public class GameData
{
    public  DateTime PreviousLogIn;
    public  Language ActiveLanguage;
    public  bool IsChapterTwoUnclocked;
    public  bool IsChapterThreeUnlocked;
    public  bool IsTutorialPlayed;
    public bool  IsAllChapterUnlocked;
    public  int Switch;
    public int[] Levels;
    public string[] DescriptionText;
}
