﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishTrigger : MonoBehaviour
{
    [Tooltip("Drag Fish here")]
    public FishMovement Fish;
    [Tooltip("Is it LeftTrigger?")]
    public bool IsLeftTrigger;
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player"&&!Fish.CanMove)
        {
            //if IsLeftTrigger is true then MoveRight is called
            if (IsLeftTrigger)
                Fish.MoveRight();
            //if IsLeftTrigger is false then MoveLeft is called
            else
                Fish.MoveLeft();
        }
    }
}
