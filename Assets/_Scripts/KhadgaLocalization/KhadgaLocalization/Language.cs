﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Khadga.Localization
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Language
    {
        NULL=100,
        ENGLISH=101,
        SPANISH=102,
        CHINESE=103,
        ARABIC =104,
        JAPANESE =105

    }

    [Serializable]
    public enum LanguageKey
    {
        Name,
        Description,
        Number,
        UnlockAll,
        PurchaseAll,
        BewareOfGravity,
        Gravity,
        FirstSteps,
        CelestialBodies,
        Switch,
        Back,
        NextLevel,
        Options,
        Sensitivity,
        Menu,
        Souls,
        HoldHereToMoveLeft,
        HoldHereToMoveRight,
        AdjustSensitivity,
        CollectSouls,
        Ok,
        Mute,
        UnMute,
        Demo,
        Language,
        English,
        Chinese,
        Japanese,
        Spanish,
        Arabic,
        Buy,
        Retry,
        TimeTaken,
        LivesTakenToComplete,
        LevelsCompleted,
        SoulsCompleted,
        Resume,
        Restart,
        GivePermissions,
        Pause,
        Share,
        NoAds,
        UnlimitedPlaytime,
        Paused,
        Loading,
        SwitchControls,
        UnLockAllChapters,
        GiveAudioPermissionsToEnjoyTheGame,
        ClickOntheAboveButtonToSwitchControls,
        I_AmAwsome,
        I_AmNot
    }
}