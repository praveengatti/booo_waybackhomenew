﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingDanger : MonoBehaviour
{

    public static bool canMove;

    // Use this for initialization
    void Start()
    {
        canMove = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
             canMove = true;
        }
    }

}
