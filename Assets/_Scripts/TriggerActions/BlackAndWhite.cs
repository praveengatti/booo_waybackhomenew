﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackAndWhite : MonoBehaviour
{
    public GameObject Cam;
    Color Black;
    Color White;
    // Use this for initialization
    void Start()
    {
        White = new Color(0.25f, 0.25f, 0.25f, 0f);
        Black = new Color(0f, 0f, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            StartCoroutine(TogleColor());
        }
    }

    public void ChangeColor(Color color)
    {
        Cam.GetComponent<Camera>().backgroundColor = color;
    }

    IEnumerator TogleColor()
    {
        ChangeColor(White);
        yield return new WaitForSeconds(2f);
        ChangeColor(Black);
    }

}
