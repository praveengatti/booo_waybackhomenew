﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using Khadga.UtilScripts.LocalData;

public class AchivementManger : MonoBehaviour
{
    public List<Achivement> AchievementList;
    public List<Achivement> AchievementList2;
    public GameObject Toast;
    public Text AchievementCompleted_PopUp;
    public static bool Achievements_Completed = false;
    private static AchivementManger instance;
    public int CurrentAchivementIndex = 100;
    public int CurrentChapterNo = 0;

    // Game Instance Singleton
    public static AchivementManger Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        Initialization();
    }

    void Initialization()
    {
        Toast.SetActive(false);
        AchievementCompleted_PopUp.gameObject.SetActive(false);
        AchievementList = LocalDataUtil.GetLocalDataOfType<List<Achivement>>("Achievements");
        AchievementList2 = LocalDataUtil.GetLocalDataOfType<List<Achivement>>("Achievements2");
        int count = 0;
        for (int i = 0; i < 10; i++)
        {
            if (AchievementList[i].IsCompleted)
                count++;
        }
        if (count >= 10)
            Achievements_Completed = true;
    }

    [System.Serializable]
    public class Achivement
    {
        public bool IsCompleted = false;
        public string Prefix;
        public string Sufix;
        public int Amount;
        public int progress = 0;
        public string AchievementGooglePlayId;
    }

    public void CheckForAchivement(Achivement achivement)
    {
        try
        {
            SaveManager.Instance.SaveData();
            if (achivement != null && achivement.IsCompleted != true)
            {
                if (achivement.progress == achivement.Amount - 1)
                {
                    OnAchivementCompleted(achivement);
                }
                else
                    achivement.progress++;
            }
          
        }
        catch (Exception e)
        {
            Debug.LogException(e);
        }
    }

    public void OnAchivementCompleted(Achivement achivement)
    {
        if (achivement != null)
        {
            GooglePlayGamesManager.Instance.GooglePlayAchivementCheck(achivement.AchievementGooglePlayId);
            achivement.IsCompleted = true;
            Debug.Log("Achievement Completed");
            StartCoroutine(Display_PopUp(achivement.Prefix, achivement.Sufix));
            SaveManager.Instance.SavePlayerData();
        }
    }

    IEnumerator Display_PopUp(string str1, string str2)
    {
        Toast.SetActive(true);
        AchievementCompleted_PopUp.gameObject.SetActive(true);
        AchievementCompleted_PopUp.text = "Achievement: " + "'" + str1 + str2 + "'" + "is Completed";
        yield return new WaitForSeconds(2);
        AchievementCompleted_PopUp.gameObject.SetActive(false);
        Toast.SetActive(false);
    }

    public void UnlockAchivement(int AchivementIndex, int ChapterNo)
    {
        if (AchivementIndex < 10)
            if (ChapterNo == 1)
            {
                AchivementManger.instance.AchievementList[AchivementIndex].progress = AchivementManger.instance.AchievementList[AchivementIndex].Amount - 1;
                CheckForAchivement(AchivementManger.instance.AchievementList[AchivementIndex]);

            }
            else if (ChapterNo == 2)
            {
                AchivementManger.instance.AchievementList2[AchivementIndex].progress = AchivementManger.instance.AchievementList2[AchivementIndex].Amount - 1;
                CheckForAchivement(AchivementManger.instance.AchievementList2[AchivementIndex]);

            }
            else
            {
                Debug.Log("given worng chapter no ");
            }

    }

    public void SetAchivemnt(Achivement achivement)
    {
        achivement.progress = 0;
    }
}