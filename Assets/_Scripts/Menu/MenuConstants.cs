﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MenuConstants  {

  //  public static string CHAPTER2_UNLOCK_STATUS="Chapter2_Ul", CHAPTER3_UNLOCK_STATUS="Chapter3_Ul";
    public static int CHAPTER1_INDEX = 0, CHAPTER2_INDEX = 1, CHAPTER3_INDEX = 2;
    public static string CH2_ANIM_FRWD = "Chapter2", CH2_ANIM_BKWD = "Chapter2back",MAINMENU_SCENENAME="MainMenu";
}
