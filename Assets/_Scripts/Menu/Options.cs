﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.Localization;
using MoreMountains.CorgiEngine;
using Khadga.UtilScripts.LocalData;


public class Options : MonoBehaviour {

    public GameObject options_panel, Mute_Button, UnMute_Button, Languages_Panel,Share_Button;
    string subject = "Check out our new game Booo";
    string body = "   https://play.google.com/store/apps/details?id=com.FreakSpace.FreakFall&hl=en";

    // Use this for initialization
    void Start ()
    {
        Initialization();
        
    }
	
    void Initialization()
    {
        options_panel.SetActive(false);
        Mute_Button.SetActive(true);
        UnMute_Button.SetActive(false);
    }

	// Update is called once per frame
	void Update () {

	}

    void Mute_unmute(bool b1,bool b2)
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        Khadga.FreakSpace.SoundManager.Instance.ToggleMute();
        Mute_Button.SetActive(b1);
        UnMute_Button.SetActive(b2);
    }

    public void Mute()
    {
        Mute_unmute(false, true);
    }

    public void UnMute()
    {
        Mute_unmute(true, false);
    }

    public void Load_Options()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
       // GameObject.Find("Menu").SetActive(false);
        options_panel.SetActive(true);
    }

    public void Load_Lanuages()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        Languages_Panel.SetActive(true);
    }

    public void Share()
    {
        OnAndroidTextSharingClick();
    }

    //RATE AND SHARE CODE FROM HERE(DUMPED FROM NET)

#if UNITY_IPHONE

[DllImport("__Internal")]
private static extern void sampleMethod (string iosPath, string message);

[DllImport("__Internal")]
private static extern void sampleTextMethod (string message);

#endif

    public void OnAndroidTextSharingClick()
    {

        StartCoroutine(ShareAndroidText());

    }
    IEnumerator ShareAndroidText()
    {
        yield return new WaitForEndOfFrame();
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID
        //Reference of AndroidJavaClass class for intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        //Reference of AndroidJavaObject class for intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        //call setAction method of the Intent object created
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        //set the type of sharing that is happening
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");
        //add data to be passed to the other activity i.e., the data to be sent
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
        //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), "Text Sharing ");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);
        //get the current activity
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        //start the activity by sending the intent data
        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
        currentActivity.Call("startActivity", jChooser);
#endif
    }


    public void OniOSTextSharingClick()
    {

#if UNITY_IPHONE || UNITY_IPAD
string shareMessage = "Wow I Just Share Text ";
sampleTextMethod (shareMessage);

#endif
    }

    public void SetLang(int Index)
    {
        switch (Index)
        {
            case 0:
                {
                    LocalizationManager.ActiveLanguage = Language.ENGLISH;
                    break;
                }
            case 1:
                {
                    LocalizationManager.ActiveLanguage = Language.CHINESE;
                    break;
                }
            case 2:
                {
                    LocalizationManager.ActiveLanguage = Language.JAPANESE;
                    break;
                }
            case 3:
                {
                    LocalizationManager.ActiveLanguage = Language.SPANISH;
                    break;
                }
            case 4:
                {
                    LocalizationManager.ActiveLanguage = Language.ARABIC;
                    break;
                }
                
        }
        DataManager.instance.gameData.ActiveLanguage = LocalizationManager.ActiveLanguage;
        DataManager.instance.SaveData();
    }

}
