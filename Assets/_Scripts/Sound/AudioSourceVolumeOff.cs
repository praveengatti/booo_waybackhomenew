﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceVolumeOff : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        foreach (var booo in FindObjectsOfType<BoooController>())
        {
            booo.GetComponent<AudioSource>().volume = 0;
        }
    }
}
