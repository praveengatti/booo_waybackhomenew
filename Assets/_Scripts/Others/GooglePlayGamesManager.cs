﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public class GooglePlayGamesManager : MonoBehaviour
{
    public static GooglePlayGamesManager Instance;
    string LeaderBoardsID = "CgkIxbbEzcEcEAIQAA";
    private void Awake()
    {
        // Debug.LogError("print");
        if (Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
       //PlayGamesPlatform.Activate();

        Debug.Log("google authentication called");
        Social.localUser.Authenticate((bool success) =>
        {
            Debug.Log("login status:" + success);
        });
    }
    public void Start()
    {
        if (!Social.localUser.authenticated)
        {
            Authenticate();
        }
    }

    public void Authenticate()
    {
        Debug.Log("Attempting to Login PlayServices");
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("PlayServices Login success");
            }
            else
            {
                Debug.Log("PlayServices Login failed");

            }
            Debug.Log("=====================" + Social.localUser.userName);
        });
    }

    // Play Services achievement
    public void GooglePlayAchivementCheck(string achivementId)
    {
        if(Social.localUser.authenticated)
        Social.ReportProgress(achivementId, 100.0f, (bool success) => { });
    }
   

}
