﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MoreMountains.CorgiEngine;
using Khadga.Localization;
using Khadga.FreakSpace;

using Khadga.Utility.Time;

public class PlayerManager : MonoBehaviour {

    public  float Timetaken;
    public int StarCount = 0;
    public Text TimeText,RandomText;
    public GameObject star1, star2, star3;
    public Slider Progressbar;
    public int ReviveCount;
    public Text Level_name;
    public GameObject LoadingPanel;
    public Text LoadingValue;
    private static PlayerManager instance;

    public Language ActiveLanguage;

    // Game Instance Singleton
    public static PlayerManager Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void OnEnable()
    {
        Health.CharacterDead += IncreamentReviveCount;
    }

    public void OnDisable()
    {
        Health.CharacterDead -= IncreamentReviveCount;
    }

    void IncreamentReviveCount()
    {
        ReviveCount++;
    }

    // Use this for initialization
    void Start () {

        Initialize();
    }

    void Initialize()
    {
        ReviveCount = 0;
        Progressbar.minValue = 0;
        Progressbar.maxValue = 100;
        LoadingPanel.SetActive(false);
        RandomText.enabled = false;
        star1.SetActive(false);
        star2.SetActive(false);
        star3.SetActive(false);
    }
	
    public void DisplayLevelName(string str)
    {
        Level_name.text = str.ToUpper();
        Level_name.gameObject.SetActive(true);
        StartCoroutine(Wait(1.5f));
    }

	// Update is called once per frame
	void Update () {

        Timetaken += Time.deltaTime;
        TimeText.text = TimeFormatUtil.SecondToHHMMSSmm(Timetaken);
    }

    //Displays Stars at the end of the game
    public int DisplayStars()
    {
         StarCount = 0;
        if(SoulsConstraint.SoulsCollected==true)
        {
            StarCount++;
        }
        if(Timetaken<=60)
        {
            StarCount++;
        }
        if(ReviveCount==0)
        {
            StarCount++;
        }

        if(StarCount==0)
        {
            EnableStars(false, false, false);
        }
        else if(StarCount==1)
        {
            EnableStars(true, false, false);
        }
        else if (StarCount == 2)
        {
            EnableStars(true,true, false);
        }
        else if (StarCount == 3)
        {
            EnableStars(true,true, true);
        }
        return StarCount;
    }

    IEnumerator Wait(float n)
    {
        yield return new WaitForSeconds(n);
        Level_name.gameObject.SetActive(false);
    }

    public IEnumerator Async_Operation(string str1,string str2)
    {
        LoadingPanel.SetActive(true);
        RandomText.text = "" + DataManager.instance.gameData.DescriptionText[Random.Range(0, DataManager.instance.gameData.DescriptionText.Length-1)];
        RandomText.enabled = true;
        AsyncOperation async = SceneManager.LoadSceneAsync(str1);
       // DisplayLevelName(str2);
        while (!async.isDone)
        {
            Progressbar.value = (async.progress/9) * 1000;
            PlayerManager.Instance.LoadingValue.text = Mathf.Ceil((async.progress/9)*1000) + "%";
            yield return null;
        }
        RandomText.enabled = false;
    }

    void EnableStars(bool b1,bool b2,bool b3)
    {
        
        star1.SetActive(b1);
        star2.SetActive(b2);
        star3.SetActive(b3);
    }

}
