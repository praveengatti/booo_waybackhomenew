﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterRight : MonoBehaviour
{
    bool CanMove;
    public GameObject Water;
    public int MovementSpeed;
    public float distanceToBeMoved;
    float x;
    bool alreadyTriggered;
    // Use this for initialization
    void Start()
    {
        alreadyTriggered = false;
        CanMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanMove)
        {
            if (Water.transform.position.x <= (x + distanceToBeMoved))
            {
                Water.transform.Translate(Vector2.down * MovementSpeed * Time.deltaTime);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!alreadyTriggered)
        {
            x = Water.transform.position.x;
            if (collision.gameObject.tag == "Player")
            {
                CanMove = true;
                alreadyTriggered = true;
            }
        }
    }
}
