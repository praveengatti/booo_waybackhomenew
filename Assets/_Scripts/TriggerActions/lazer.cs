﻿using UnityEngine;
using System.Collections;
using MoreMountains.CorgiEngine;

public class lazer : MonoBehaviour
{
    Health Health;
    private LineRenderer LineRenderer;
    // Use this for initialization
    void Start()
    {
        LineRenderer = GetComponent<LineRenderer>();
        Health = FindObjectOfType<Health>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        LineRenderer.SetPosition(0, transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.right, out hit))
        {
            if (hit.collider)
            {
                LineRenderer.SetPosition(1, hit.point);
            }

            if(hit.transform.gameObject.tag=="Player")
            {
                
                Health.Damage(1, this.gameObject, 0, 0);
            }
        }
        else LineRenderer.SetPosition(1, transform.right * 150);
    }
}