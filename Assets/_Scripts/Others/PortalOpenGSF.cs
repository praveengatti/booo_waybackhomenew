﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalOpenGSF : MonoBehaviour {
    public GameObject EndPortal;
    public GameObject GS1;
    public int GS1V;
    public GameObject GS2;
    public int GS2V;
    public GameObject PEffect1f;
    public GameObject PEffect2f;
    public GameObject PEffect3f;
    public GameObject PEffect4f;
    public GameObject PEffect1r;
    public GameObject PEffect2r;
    public GameObject PEffect3r;
    public GameObject PEffect4r;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        PEffect1f.SetActive(false);
        PEffect2f.SetActive(false);
        PEffect1r.SetActive(true);
        PEffect2r.SetActive(true);
        PEffect3f.SetActive(false);
        PEffect4f.SetActive(false);
        PEffect3r.SetActive(true);
        PEffect4r.SetActive(true);
        EndPortal.SetActive(true);
        GS1.GetComponent<GravitySwitch>().TargetAngle = GS1V;
        GS2.GetComponent<GravitySwitch>().TargetAngle = GS2V;
    }
}
