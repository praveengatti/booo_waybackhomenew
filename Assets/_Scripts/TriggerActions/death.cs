﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using System;
using Khadga.FreakSpace;
public class death : MonoBehaviour
{
    Health Health;
    //public Animator anim;
    // Use this for initialization
    public void Start()
    {
        Health = FindObjectOfType<Health>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            DoorCloser.CanCloseDoor = false;
            MovingDanger.canMove = false;
            RandomInstantiate.canInst = false;
            
            //anim.SetTrigger("Poof");
            Health.Damage(1, this.gameObject, 0, 0);
            if (this.gameObject.tag == "water")
            {
                Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[8].Name);
                if (MainMenu.Chapter_Index == 0)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[4]);
                }
                else if(MainMenu.Chapter_Index==1)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[4]);
                }
            }
            else if (this.gameObject.tag == "Spike")
            {
                 Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[7].Name);
                if (MainMenu.Chapter_Index == 0)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[3]);
                }
                else if (MainMenu.Chapter_Index == 1)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[3]);
                }
            }
            else if (this.gameObject.tag == "Roller")
            {
                 Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[9].Name);
                if (MainMenu.Chapter_Index == 0)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[5]);
                }
                else if (MainMenu.Chapter_Index == 1)
                {
                    if (AchivementManger.Instance != null)
                        AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[5]);
                }
            }
        }
    }
}
