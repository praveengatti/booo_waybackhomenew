﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingJunk : MonoBehaviour
{

    public float movementSpeed;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(MovingDanger.canMove)
        {
            this.transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
        }
    }
}
