﻿///
///this class is used to manage the lever and moving spikes in the   level 9 GS
///


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManger : MonoBehaviour {


    //edit needed
    [Tooltip("Spike")]
    public Animator Spike1;
    [Tooltip("Spike")]
    public Animator Spike2;
    [Tooltip("Spike")]
    public Animator Spike3;
    [Tooltip("Spike")]
    public Animator Spike4;
    [Tooltip("LeverOne")]
    public Animator Lever1;
    [Tooltip("LeverTwo")]
    public Animator Lever2;
    [Tooltip("LeverThree")]
    public Animator Lever3;
    [Tooltip("LeverFour")]
    public Animator Lever4;


    const string PUZZLE_LEVER_1 = "PuzzleLever1";
    const string PUZZLE_LEVER_2 = "PuzzleLever2";
    const string PUZZLE_LEVER_3 = "PuzzleLever3";
    const string PUZZLE_LEVER_4 = "PuzzleLever4";

    private void Start()
    {
        Spike2.SetBool("IsOpen", true);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        string nameOfTheLever = this.gameObject.tag;
        switch (nameOfTheLever)
        {
            case PUZZLE_LEVER_1:

                if (collision.gameObject.tag == "Player")
                {
                    LeverComplement(Lever1);
                    Complement(Spike1);
                }
                break;
            case PUZZLE_LEVER_2:

                if (collision.gameObject.tag == "Player")
                {
                    LeverComplement(Lever2);
                    Complement(Spike1, Spike2);
                }
                break;
            case PUZZLE_LEVER_3:

                if (collision.gameObject.tag == "Player")
                {
                    LeverComplement(Lever3);
                    Complement(Spike3, Spike2);
                }
                break;
            case PUZZLE_LEVER_4:

                if (collision.gameObject.tag == "Player")
                {
                    LeverComplement(Lever4);
                    Complement(Spike1, Spike2, Spike3, Spike4);
                }
                break;

        }

    }
    public void Complement(Animator spike)
    {
        spike.SetBool("IsOpen", !spike.GetBool("IsOpen"));
    }
    public void Complement(Animator spike, Animator spike2)
    {
        Complement(spike);
        Complement(spike2);
    }
    public void Complement(Animator spike, Animator spike2, Animator spike3, Animator spike4)
    {
        Complement(spike);
        Complement(spike2);
        Complement(spike3);
        Complement(spike4);
    }
    public void LeverComplement(Animator Lever)
    {
        Lever.SetBool("IsTriggered", !Lever.GetBool("IsTriggered"));
    }
}
