﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class SoulsConstraint : MonoBehaviour
{
    public int CollectibleConstraint;
    public static int SoulsToBeCollected;
    public static bool SoulsCollected;

    // Use this for initialization
    void Start()
    {
        if (PlayerManager.Instance != null)
            PlayerManager.Instance.LoadingPanel.SetActive(false);
        SoulsToBeCollected = CollectibleConstraint;
        SoulsCollected = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(SoulManager.SoulsCollected>=CollectibleConstraint)
        {
            SoulsCollected = true;
        }
    }
}
