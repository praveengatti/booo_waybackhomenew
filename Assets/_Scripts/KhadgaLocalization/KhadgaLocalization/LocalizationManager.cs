﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Khadga.UtilScripts.LocalData;
using UnityEngine.UI;
namespace Khadga.Localization
{
    public static class LocalizationManager
    {
        public static Languages Languages;
        public static Language ActiveLanguage;
        public static void SetLanguages()
        {
            Debug.Log("languages displaying");
            Debug.Log(LocalDataUtil.GetLocalDataOfType<Languages>("Languages"));
            Debug.Log("languages displaying done");
            Languages = LocalDataUtil.GetLocalDataOfType<Languages>("Languages");
        }

        //public static void GetLanguages()
        //{
        //    Languages = new Languages();
        //    Languages.Add(Language.ENGLISH,new Phrases());
        //    LocalDataUtil.SaveToLocal<Languages>(Languages,"Languages");
        //}

        public static string GetTextForKey(LanguageKey languagekey)
        {

            switch (languagekey)
            {
                case LanguageKey.AdjustSensitivity:
                    return Languages[ActiveLanguage].AdjustSensitivity;
                case LanguageKey.Back:
                    return Languages[ActiveLanguage].Back;
                case LanguageKey.BewareOfGravity:
                    return Languages[ActiveLanguage].BewareOfGravity;
                case LanguageKey.CelestialBodies:
                    return Languages[ActiveLanguage].CelestialBodies;
                case LanguageKey.CollectSouls:
                    return Languages[ActiveLanguage].CollectSouls;
                case LanguageKey.Description:
                    return Languages[ActiveLanguage].Description;
                case LanguageKey.FirstSteps:
                    return Languages[ActiveLanguage].FirstSteps;
                case LanguageKey.Gravity:
                    return Languages[ActiveLanguage].Gravity;
                case LanguageKey.HoldHereToMoveLeft:
                    return Languages[ActiveLanguage].HoldHereToMoveLeft;
                case LanguageKey.HoldHereToMoveRight:
                    return Languages[ActiveLanguage].HoldHereToMoveRight;
                case LanguageKey.Menu:
                    return Languages[ActiveLanguage].Menu;
                case LanguageKey.Name:
                    return Languages[ActiveLanguage].Name;
                case LanguageKey.NextLevel:
                    return Languages[ActiveLanguage].NextLevel;
                case LanguageKey.Number:
                    return Languages[ActiveLanguage].Number;
                case LanguageKey.Options:
                    return Languages[ActiveLanguage].Options;
                case LanguageKey.PurchaseAll:
                    return Languages[ActiveLanguage].PurchaseAll;
                case LanguageKey.Sensitivity:
                    return Languages[ActiveLanguage].Sensitivity;
                case LanguageKey.Souls:
                    return Languages[ActiveLanguage].Souls;
                case LanguageKey.Switch:
                    return Languages[ActiveLanguage].Switch;
                case LanguageKey.UnlockAll:
                    return Languages[ActiveLanguage].UnlockAll;
                case LanguageKey.Ok:
                    return Languages[ActiveLanguage].Ok;
                case LanguageKey.Mute:
                    return Languages[ActiveLanguage].Mute;
                case LanguageKey.UnMute:
                    return Languages[ActiveLanguage].UnMute;
                case LanguageKey.Demo:
                    return Languages[ActiveLanguage].Demo;
                case LanguageKey.Language:
                    return Languages[ActiveLanguage].Language;
                case LanguageKey.English:
                    return Languages[ActiveLanguage].English;
                case LanguageKey.Chinese:
                    return Languages[ActiveLanguage].Chinese;
                case LanguageKey.Japanese:
                    return Languages[ActiveLanguage].Japanese;
                case LanguageKey.Spanish:
                    return Languages[ActiveLanguage].Spanish;
                case LanguageKey.Arabic:
                    return Languages[ActiveLanguage].Arabic;
                case LanguageKey.Buy:
                    return Languages[ActiveLanguage].Buy;
                case LanguageKey.Retry:
                    return Languages[ActiveLanguage].Retry;
                case LanguageKey.TimeTaken:
                    return Languages[ActiveLanguage].TimeTaken;
                case LanguageKey.LivesTakenToComplete:
                    return Languages[ActiveLanguage].LivesTakenToComplete;
                case LanguageKey.LevelsCompleted:
                    return Languages[ActiveLanguage].LevelsCompleted;
                case LanguageKey.SoulsCompleted:
                    return Languages[ActiveLanguage].SoulsCompleted;
                case LanguageKey.Resume:
                    return Languages[ActiveLanguage].Resume;
                case LanguageKey.Restart:
                    return Languages[ActiveLanguage].Restart;
                case LanguageKey.GivePermissions:
                    return Languages[ActiveLanguage].GivePermissions;
                case LanguageKey.Pause:
                    return Languages[ActiveLanguage].Pause;
                case LanguageKey.Share:
                    return Languages[ActiveLanguage].Share;
                case LanguageKey.NoAds:
                    return Languages[ActiveLanguage].NoAds;
                case LanguageKey.UnlimitedPlaytime:
                    return Languages[ActiveLanguage].UnlimitedPlaytime;
                case LanguageKey.Paused:
                    return Languages[ActiveLanguage].Paused;
                case LanguageKey.SwitchControls:
                    return Languages[ActiveLanguage].SwitchControls;
                case LanguageKey.Loading:
                    return Languages[ActiveLanguage].Loading;
                case LanguageKey.UnLockAllChapters:
                    return Languages[ActiveLanguage].UnLockAllChapters;
                case LanguageKey.GiveAudioPermissionsToEnjoyTheGame:
                    return Languages[ActiveLanguage].GiveAudioPermissionsToEnjoyTheGame;
                case LanguageKey.ClickOntheAboveButtonToSwitchControls:
                    return Languages[ActiveLanguage].ClickOntheAboveButtonToSwitchControls;
                case LanguageKey.I_AmAwsome:
                    return Languages[ActiveLanguage].I_AmAwsome;
                case LanguageKey.I_AmNot:
                    return Languages[ActiveLanguage].I_AmNot;
                default:
                    return null;
                  
                 
            }

        }

        public static Language SystemLang()
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Arabic:
                    {
                        return Language.ARABIC;
                        break;
                    }
                case SystemLanguage.Chinese:
                    {
                        return Language.CHINESE;
                        break;
                    }
                case SystemLanguage.Spanish:
                    {
                        return Language.SPANISH;
                        break;
                    }
                case SystemLanguage.Japanese:
                    {
                        return Language.JAPANESE;
                        break;
                    }
                default:
                    {
                        return Language.ENGLISH;
                    }

            }
        }

        public static string GetTextInEnglishForKey(LanguageKey languagekey)
        {

            switch (languagekey)
            {
                case LanguageKey.AdjustSensitivity:
                    return Languages[Language.ENGLISH].AdjustSensitivity;
                case LanguageKey.Back:
                    return Languages[Language.ENGLISH].Back;
                case LanguageKey.BewareOfGravity:
                    return Languages[Language.ENGLISH].BewareOfGravity;
                case LanguageKey.CelestialBodies:
                    return Languages[Language.ENGLISH].CelestialBodies;
                case LanguageKey.CollectSouls:
                    return Languages[Language.ENGLISH].CollectSouls;
                case LanguageKey.Description:
                    return Languages[Language.ENGLISH].Description;
                case LanguageKey.FirstSteps:
                    return Languages[Language.ENGLISH].FirstSteps;
                case LanguageKey.Gravity:
                    return Languages[Language.ENGLISH].Gravity;
                case LanguageKey.HoldHereToMoveLeft:
                    return Languages[Language.ENGLISH].HoldHereToMoveLeft;
                case LanguageKey.HoldHereToMoveRight:
                    return Languages[Language.ENGLISH].HoldHereToMoveRight;
                case LanguageKey.Menu:
                    return Languages[Language.ENGLISH].Menu;
                case LanguageKey.Name:
                    return Languages[Language.ENGLISH].Name;
                case LanguageKey.NextLevel:
                    return Languages[Language.ENGLISH].NextLevel;
                case LanguageKey.Number:
                    return Languages[Language.ENGLISH].Number;
                case LanguageKey.Options:
                    return Languages[Language.ENGLISH].Options;
                case LanguageKey.PurchaseAll:
                    return Languages[Language.ENGLISH].PurchaseAll;
                case LanguageKey.Sensitivity:
                    return Languages[Language.ENGLISH].Sensitivity;
                case LanguageKey.Souls:
                    return Languages[Language.ENGLISH].Souls;
                case LanguageKey.Switch:
                    return Languages[Language.ENGLISH].Switch;
                case LanguageKey.UnlockAll:
                    return Languages[Language.ENGLISH].UnlockAll;
                case LanguageKey.Ok:
                    return Languages[Language.ENGLISH].Ok;
                case LanguageKey.Mute:
                    return Languages[Language.ENGLISH].Mute;
                case LanguageKey.UnMute:
                    return Languages[Language.ENGLISH].UnMute;
                case LanguageKey.Demo:
                    return Languages[Language.ENGLISH].Demo;
                case LanguageKey.Language:
                    return Languages[Language.ENGLISH].Language;
                case LanguageKey.English:
                    return Languages[Language.ENGLISH].English;
                case LanguageKey.Chinese:
                    return Languages[Language.ENGLISH].Chinese;
                case LanguageKey.Japanese:
                    return Languages[Language.ENGLISH].Japanese;
                case LanguageKey.Spanish:
                    return Languages[Language.ENGLISH].Spanish;
                case LanguageKey.Arabic:
                    return Languages[Language.ENGLISH].Arabic;
                case LanguageKey.Buy:
                    return Languages[Language.ENGLISH].Buy;
                case LanguageKey.Retry:
                    return Languages[Language.ENGLISH].Retry;
                case LanguageKey.TimeTaken:
                    return Languages[Language.ENGLISH].TimeTaken;
                case LanguageKey.LivesTakenToComplete:
                    return Languages[Language.ENGLISH].LivesTakenToComplete;
                case LanguageKey.LevelsCompleted:
                    return Languages[Language.ENGLISH].LevelsCompleted;
                case LanguageKey.SoulsCompleted:
                    return Languages[Language.ENGLISH].SoulsCompleted;
                case LanguageKey.Resume:
                    return Languages[Language.ENGLISH].Resume;
                case LanguageKey.Restart:
                    return Languages[Language.ENGLISH].Restart;
                case LanguageKey.GivePermissions:
                    return Languages[Language.ENGLISH].GivePermissions;
                case LanguageKey.Pause:
                    return Languages[Language.ENGLISH].Pause;
                case LanguageKey.Share:
                    return Languages[Language.ENGLISH].Share;
                case LanguageKey.NoAds:
                    return Languages[Language.ENGLISH].NoAds;
                case LanguageKey.UnlimitedPlaytime:
                    return Languages[Language.ENGLISH].UnlimitedPlaytime;
                case LanguageKey.Paused:
                    return Languages[Language.ENGLISH].Paused;
                case LanguageKey.SwitchControls:
                    return Languages[Language.ENGLISH].SwitchControls;
                case LanguageKey.Loading:
                    return Languages[Language.ENGLISH].Loading;
                case LanguageKey.UnLockAllChapters:
                    return Languages[Language.ENGLISH].UnLockAllChapters;
                case LanguageKey.GiveAudioPermissionsToEnjoyTheGame:
                    return Languages[Language.ENGLISH].GiveAudioPermissionsToEnjoyTheGame;
                case LanguageKey.ClickOntheAboveButtonToSwitchControls:
                    return Languages[Language.ENGLISH].ClickOntheAboveButtonToSwitchControls;
                case LanguageKey.I_AmAwsome:
                    return Languages[Language.ENGLISH].I_AmAwsome;
                case LanguageKey.I_AmNot:
                    return Languages[Language.ENGLISH].I_AmNot;
                default:
                    return null;
            }
        }

    }
}
