﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomInstantiate : MonoBehaviour
{
    public GameObject prefab;
    public Transform Left;
    public Transform right;
    public static bool canInst;
    // Use this for initialization
    void Start()
    {
        canInst = false;
        InvokeRepeating("RandomInst", 0f, .5f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void RandomInst()
    {
        if (canInst)
        {
            var position = new Vector3(Random.Range(Left.position.x, right.position.x), 72.8f, 0);
            Instantiate(prefab, position, Quaternion.identity);
        }
    }
}