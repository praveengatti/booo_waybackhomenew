﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using Khadga.UtilScripts.LocalData;

public class StarsAchievement : MonoBehaviour {

    int value;

    // Use this for initialization
    void Start () {
        NumberOfStarsAchieved();
      
    }

    void CheckForAchievement()
    {
        if (MainMenu.Chapter_Index == 0)
        {
            if (AchivementManger.Instance != null)
                AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[2]);
        }
        else if (MainMenu.Chapter_Index == 1)
        {
            if (AchivementManger.Instance != null)
                AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[2]);
        }
    }
	
    void NumberOfStarsAchieved()
    {
        SaveManager.Instance.Levels = DataManager.instance.gameData.Levels; //LocalDataUtil.GetLocalDataOfType<int[]>("LevelInfo");
      
         value = 0;
        for(int i=1; i<=10;i++)
        {
            if(SaveManager.Instance.Levels[i]>=0)
            value += SaveManager.Instance.Levels[i];
        }
        if (value >= 25)
            CheckForAchievement();
        value = 0;
        for (int i = 11; i <= 20; i++)
        {
            if (SaveManager.Instance.Levels[i] >= 0)
                value += SaveManager.Instance.Levels[i];
        }
        if (value >= 25)
            CheckForAchievement();
    }

	// Update is called once per frame
	void Update () {
		
	}
}
