﻿using UnityEngine;
using System.Collections;

public class CircularMovement : MonoBehaviour
{

    public Transform center;
    public float degreesPerSecond = -65.0f;
    //public bool Enable=false;

    private Vector3 v;

    void Start()
    {

        v = transform.position - center.position;
    }

    void Update()
    {
            //transform.RotateAround(center.transform.position, Vector3.forward, degreesPerSecond);
            v = Quaternion.AngleAxis(degreesPerSecond * Time.deltaTime, Vector3.forward) * v;
            transform.position = center.position + v;
    }
}