﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosingDoors : MonoBehaviour
{
    public float movementSpeed;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(DoorCloser.CanCloseDoor)
        {
            this.transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    this.GetComponent<Rigidbody2D>().
    //}
}
