﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLift : MonoBehaviour
{
    public Animator DoorLif;
    public Animator lever;
    public bool IsCameraMovementNeeded=false;
    public float Duration;

    // Use this for initialization
    void Start()
    {
        try
        {
            if (DoorLif == null) ;
        }
        catch(System.Exception e)
        {

        }
     
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void action()
    {
        if (DoorLif != null)
        {
            DoorLif.SetTrigger("Start");
            StartCoroutine(Wait(Duration));
            
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            lever.SetTrigger("LeverStart");
            if (IsCameraMovementNeeded)
            {
                this.gameObject.GetComponent<CameraMovementOnTrigger>().Canvas.enabled = false;
                this.gameObject.GetComponent<CameraMovementOnTrigger>().Camera.SetActive(true);
                this.gameObject.GetComponent<CameraMovementOnTrigger>().MainCamera.SetActive(false);
                this.gameObject.GetComponent<CameraMovementOnTrigger>().Camera.gameObject.transform.position = this.GetComponent<CameraMovementOnTrigger>().MainCamera.transform.position;
                this.gameObject.GetComponent<CameraMovementOnTrigger>().IntialTransform = this.gameObject.GetComponent<CameraMovementOnTrigger>().Camera.transform.position;
                StartCoroutine(Wait(Duration));
                this.gameObject.GetComponent<CameraMovementOnTrigger>().CameraAction = true;
            }
            else
            {
                action();
            }
        
        }
    }

    IEnumerator Wait(float duration)
    {
        yield return new WaitForSeconds(duration);
            this.GetComponent<CameraMovementOnTrigger>().BackTracing = true;
    }

}
