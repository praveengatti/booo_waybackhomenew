﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.IO;
using System;
namespace Khadga.UtilScripts.LocalData
{
    //
    // Summary:
    //     ///
    //      Utility to save to and load data from local 
    //     /// 

    public static class LocalDataUtil
    {
        public static string Path=Application.persistentDataPath;
        //
        // Summary:
        //     ///
        //      Utility to save data to local 
        //     ///

        public static void SaveToLocal<T>(T obj, string fileName)
        {
            if (obj == null)
            {
                return;
            }

            string json = JsonConvert.SerializeObject(obj);
            File.WriteAllText(Path+ "/" + fileName, json);
           Debug.Log(fileName + " :Saved at" + Path);
        }
        //
        // Summary:
        //     ///
        //      Utility to  load data from local 
        //     ///

        public static T GetLocalDataOfType<T>(string fileName)
        {
            string filePath = Path + "/" + fileName;
            T data;
            string json;
            try
            {
                json = File.ReadAllText(filePath);

                data = JsonConvert.DeserializeObject<T>(json);
            }

            // if file is not found load data from resouces/data/filename
            catch (Exception e)
            {
                json = Resources.Load<TextAsset>("Data/"+fileName).text;

                 data = JsonConvert.DeserializeObject<T>(json);
            }
            return data;

        }

    }
}



