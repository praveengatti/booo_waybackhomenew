﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenSoul : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (MainMenu.Chapter_Index == 0)
            {
                if (AchivementManger.Instance != null)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[9]);
            }
            else if (MainMenu.Chapter_Index == 1)
            {
                if (AchivementManger.Instance != null)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[9]);
            }
        }
    }
}
