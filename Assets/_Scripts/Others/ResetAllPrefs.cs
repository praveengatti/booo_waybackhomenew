﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAllPrefs : MonoBehaviour {

	void Start ()
    {
        PlayerPrefs.DeleteAll();
	}
}
