﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;

public class GameEndPopUp : MonoBehaviour {

    public GameObject gameoverpanel,gameoverpanel2;
    public GameObject nextlevel;
    public Text TimeTaken;
    public Text ReviveCount,SoulsToBeColllected;

    private static GameEndPopUp instance;

    // Game Instance Singleton
    public static GameEndPopUp Instance
    {
        get
        {
            return instance;
        }
    }

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        Initialization();
    }
	
    void Initialization()
    {
        PlayerManager.Instance.RandomText.enabled = false;
        gameoverpanel.SetActive(false);
        gameoverpanel2.SetActive(false);
    }

	// Update is called once per frame
	void Update () {

      
    }

    //Loads NextLevel
    public void NextLevel()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        if (MainMenu.Level_Index != 10 && MainMenu.Level_Index != 20 &&
            MainMenu.Level_Index != 35)
        {
            Reset();
            MainMenu.Level_Index += 1;
            // gets the curent screen
            Scene sceneloaded = SceneManager.GetActiveScene();
            StartCoroutine(Async_Operation(sceneloaded.buildIndex+1));
           // PlayerManager.Instance.DisplayLevelName(SceneManager.GetSceneByBuildIndex(sceneloaded.buildIndex+1).name);
        }
    }

    IEnumerator Async_Operation(int sceneIndex)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneIndex);
        while(!async.isDone)
        {
           PlayerManager.Instance.RandomText.text = "" + DataManager.instance.gameData.DescriptionText[Random.Range(0, DataManager.instance.gameData.DescriptionText.Length - 1)];
            PlayerManager.Instance.RandomText.enabled = true;
            PlayerManager.Instance.Progressbar.value = (async.progress/9) * 1000;
            PlayerManager.Instance.LoadingValue.text = Mathf.Ceil((async.progress/9) * 1000) + "%";
            yield return null;
        }
        PlayerManager.Instance.RandomText.enabled = false;
    }

    //restarts the Present Level
    public void Replay()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Reset();
    }

    //resets the player data
    public void Reset()
    {
        PlayerManager.Instance.ReviveCount = 0;
        PlayerManager.Instance.Timetaken = 0;
        gameoverpanel.SetActive(false);
        gameoverpanel2.SetActive(false);
        PlayerManager.Instance.LoadingPanel.SetActive(true);
        Pause.Instance.pauseButton.SetActive(true);
        Pause.Instance.SoulsPanel.SetActive(true);
    }

    //Loads MainMenu
    public void LoadMainMenu()
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[3].Name);
        Time.timeScale = 1;
        gameoverpanel.SetActive(false);
        gameoverpanel2.SetActive(false);
        Pause.Instance.PauseMenu.SetActive(false);
        StartCoroutine(PlayerManager.Instance.Async_Operation("MainMenu","MainMenu"));
    }
}

