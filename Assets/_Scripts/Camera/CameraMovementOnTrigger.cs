﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementOnTrigger : MonoBehaviour {

    public GameObject Target;
    public GameObject Camera;
    public GameObject MainCamera;
    public int speed;
    public bool CameraAction = false;
    public bool BackTracing = false;
    public Vector3 IntialTransform;
    Vector3 TargetPosition;
    public Canvas Canvas;
    public bool IsReached = false;
    // Use this for initialization
    void Start()
    {
        IntialTransform = MainCamera.transform.position;
        Camera.gameObject.transform.position = MainCamera.transform.position;
        TargetPosition = Target.transform.position;
        TargetPosition.z = MainCamera.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.transform.position != TargetPosition && CameraAction == true)
        {
            Camera.transform.position = Vector3.MoveTowards(Camera.transform.position, TargetPosition, Time.deltaTime * speed);
        }
        if (Camera.transform.position == TargetPosition && BackTracing == false && !IsReached)
        {
            this.GetComponent<DoorLift>().action();
            CameraAction = false;
            IsReached = true;
        }
        if (Camera.transform.position != MainCamera.transform.position && BackTracing == true)
        {
            Camera.gameObject.transform.position = Vector3.MoveTowards(Camera.transform.position, MainCamera.transform.position, Time.deltaTime * speed);
        }
        if (Camera.transform.position == MainCamera.transform.position && BackTracing == true)
        {
            Camera.SetActive(false);
            MainCamera.SetActive(true);
            Canvas.enabled = true;
            BackTracing = false;
            this.GetComponent<DoorLift>().IsCameraMovementNeeded = false;
        }
    }
}
