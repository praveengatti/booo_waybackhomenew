﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;

public class ChapterProgression : MonoBehaviour {

    int i,count;
    public Text Chapter1_Progreesion, Chapter2_Progreesion, Chapter3_Progreesion;

    public void Start()
    {
        DisplayProgression();
    }

    void Check_Count(int m,int n,Text t)
    {
        count = 0;
        for (i = m; i <= n; i++)
        {
            if (SaveManager.Instance.Levels[i] >= 1)
            {
                count++;
            }
        }
       t.text = "" + count;
    }

    void DisplayProgression()
    {
            Check_Count(1, 10, Chapter1_Progreesion);
      
            Check_Count(11, 20, Chapter2_Progreesion);
     
            Check_Count(21, 35, Chapter3_Progreesion);
    }

}
