﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySwitch : MonoBehaviour
{

    CharacterGravity CharacterGravity;

    [Range(-180, 180)]
    public float TargetAngle;

    // Use this for initialization
    void Start()
    {
        CharacterGravity = FindObjectOfType<CharacterGravity>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag =="Player")
        CharacterGravity.InitialGravityAngle = TargetAngle;
    }
}
