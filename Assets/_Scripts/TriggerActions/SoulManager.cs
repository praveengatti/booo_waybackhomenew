﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SoulManager : MonoBehaviour
{
    public static int SoulsCollected;

    // Use this for initialization
    void Start()
    {
        SoulsCollected = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            SoulsCollected++;
            if (MainMenu.Chapter_Index == 0)
            {
                if (AchivementManger.Instance != null)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList[1]);
            }
            else if (MainMenu.Chapter_Index == 1)
            {
                if (AchivementManger.Instance != null)
                    AchivementManger.Instance.CheckForAchivement(AchivementManger.Instance.AchievementList2[1]);
            }
        }
    }
}
