﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.Localization
{
    [RequireComponent(typeof(Text))]
    public class LocalizationText : MonoBehaviour
    {
        public LanguageKey LanguageKey;
        Language PreviousLanguage;
        private void Start()
        {
            if (LocalizationManager.ActiveLanguage == Language.NULL)
            {
                LocalizationManager.ActiveLanguage = LocalizationManager.SystemLang();
            }
            SetText();
        }
        public void Update()
        {
            if (LocalizationManager.ActiveLanguage != PreviousLanguage)
            {
                SetText();
            }
        }
        public void SetText()
        {
            PreviousLanguage = LocalizationManager.ActiveLanguage;

            if (LocalizationManager.GetTextForKey(LanguageKey) != null)
                this.GetComponent<Text>().text = LocalizationManager.GetTextForKey(LanguageKey);
            else
                this.GetComponent<Text>().text = LocalizationManager.GetTextInEnglishForKey(LanguageKey);
        }
    }
}






