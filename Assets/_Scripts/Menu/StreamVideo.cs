﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour
{

    public RawImage Image;

    public VideoClip VideoToPlay;

    public Texture DefaultTexture;

    private VideoPlayer VideoPlayer;

    private AudioSource audioSource;

    bool IsVideoStarted;

    // Use this for initialization
    void Start()
    {
        IsVideoStarted = false;
        InitializeVideoPlayer();
        PrepareVideo();
    }

    void InitializeVideoPlayer()
    {
        Image.texture = DefaultTexture;

        //Add VideoPlayer to the GameObject
        VideoPlayer = gameObject.AddComponent<VideoPlayer>();

        //Add AudioSource
        audioSource = gameObject.AddComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        VideoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        VideoPlayer.isLooping = true;
        audioSource.Pause();
    }

    private void Update()
    {
        if (!IsVideoStarted)
        {
            if (!VideoPlayer.isPrepared)
            {
             //   Debug.Log("Preparing Video");

            }
            else
            {
                IsVideoStarted = true;
                //Assign the Texture from Video to RawImage to be displayed
                Image.texture = VideoPlayer.texture;

                //Play Video
                VideoPlayer.Play();

                //Play Sound
                audioSource.Play();

                Debug.Log("Playing Video");
            }
        }
    }

    void PrepareVideo()
    {
        //We want to play from video clip not from url

        VideoPlayer.source = VideoSource.VideoClip;


        //Set Audio Output to AudioSource
        VideoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        VideoPlayer.EnableAudioTrack(0, true);
        VideoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        VideoPlayer.clip = VideoToPlay;
        VideoPlayer.Prepare();
    }


}