﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCloser : MonoBehaviour
{
    public static bool CanCloseDoor;
    // Use this for initialization
    void Start()
    {
        CanCloseDoor = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            CanCloseDoor = true;
            RandomInstantiate.canInst = true;
        }
    }
}
