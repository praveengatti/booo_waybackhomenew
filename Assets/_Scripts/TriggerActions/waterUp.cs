﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterUp : MonoBehaviour
{
    public GameObject cam;
    public GameObject Water;
    bool CanMove;
    public float movementSpeed;
    public  GameObject portal;
    // Use this for initialization
    void Start()
    {
        portal.SetActive(false);
        CanMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(CanMove)
        {
            Water.transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            portal.SetActive(true);
            CanMove = true;
            cam.GetComponent<Camera>().backgroundColor = new Color(0.25f,0.25f, 0.25f, 0f);
        }
    }
}
