﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using MoreMountains.Tools;
using UnityEngine.UI.Extensions;
using Khadga.FreakSpace;

public class MainMenu : MonoBehaviour {

    public GameObject ch1,ch2,ch3,Ch2_Ul,Ch3_Ul;
    public static int Level_Index,Chapter_Index=-1;
    public GameObject Menu_Text;
    public GameObject UnlockAllChapers;
    public Text PremiumUser;
    public Transform[] ChapterButtons;

    bool IsLevelLoaded=false;
    int True = 1;

    public HorizontalScrollSnap HorizontalScrollSnap;

    public void OnChapterClick(int chapterNumber)
    {
        
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[5].Name);
        if (HorizontalScrollSnap.CurrentPage == chapterNumber)
        {
            SetChapterIndex(chapterNumber);
            LoadChapter();
        }
        else
        {
            if (chapterNumber > HorizontalScrollSnap.CurrentPage)
            {
                HorizontalScrollSnap.NextScreen();

            }
            else
                HorizontalScrollSnap.PreviousScreen();
            Debug.Log("Snap to chapter " + chapterNumber);
        }

    }


    // Use this for initialization
    void Start () {
        Initialize();
    }

    void Initialize()
    {
        ScaleUpChapterButton(0);
        SoundManager.Instance.PlayMusic(SoundManager.Instance.GameSounds[2]);
        Pause.Instance.SoulsPanel.SetActive(false);
        PremiumUser.enabled = false;
        if (PlayerManager.Instance != null)
            PlayerManager.Instance.LoadingPanel.SetActive(false);
        checkforIAP();
        Hide_AllLevels();
        LoadChapter();
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    public void checkforIAP()
    {
        if (DataManager.instance.gameData.IsChapterThreeUnlocked)
        {
            UnlockAllChapers.SetActive(false);
            PremiumUser.enabled = true;
        }
        else if (DataManager.instance.gameData.IsChapterTwoUnclocked == true && DataManager.instance.gameData.IsChapterThreeUnlocked == true)
        {
            UnlockAllChapers.SetActive(false);
            PremiumUser.enabled = true;
        }
        else if (DataManager.instance.gameData.IsChapterTwoUnclocked == true || DataManager.instance.gameData.IsChapterThreeUnlocked == true)
        {
            UnlockAllChapers.SetActive(false);
        }
        else
        {
            UnlockAllChapers.SetActive(true);
        }
    }

    void ResetChapterButtons()
    {
        foreach (var button in ChapterButtons)
        {
            button.localScale = new Vector3(1, 1, 1);
        }
    }

    void ScaleUpChapterButton(int buttonIndex)
    {
        ChapterButtons[buttonIndex].localScale = new Vector3(1.2f, 1.2f, 1.2f);
    }

    public void OnChapterScrollChange()
    {
        ResetChapterButtons();
        ScaleUpChapterButton(HorizontalScrollSnap.CurrentPage);
    }

    

    //hides all the levels
    void Hide_AllLevels()
    {
        ch1.SetActive(false);
        ch2.SetActive(false);
        ch3.SetActive(false);
        Ch2_Ul.SetActive(false);
        Ch3_Ul.SetActive(false);
    }

    void Check_Chapter_Status(bool var,GameObject ch_ul,GameObject ch)
    {
        if (var == true)
            ch_ul.SetActive(true);
        else if ( var!= true)
            ch.SetActive(true);
    }

    //Loads the chapter
    public void LoadChapter()
    {
        if (Chapter_Index == MenuConstants.CHAPTER1_INDEX)
        {
            ch1.SetActive(true);
            Menu_Text.SetActive(false);
        }
        else if (Chapter_Index == MenuConstants.CHAPTER2_INDEX)
        {
            Check_Chapter_Status(DataManager.instance.gameData.IsChapterTwoUnclocked, Ch2_Ul, ch2);
            Menu_Text.SetActive(false);
        }

        else if (Chapter_Index == MenuConstants.CHAPTER3_INDEX)
        {
            Check_Chapter_Status(DataManager.instance.gameData.IsChapterThreeUnlocked, Ch3_Ul, ch3);
            Menu_Text.SetActive(false);
        }
    }

    public void SetChapterIndex(int val)
    {
       Chapter_Index = val;
    }

    public void SetLevelIndex(int val)
    {
        Level_Index = val;
    }

    //Loads level
    public void LoadLevel(string lvl)
    {
        if (!IsLevelLoaded)
        {
            Khadga.FreakSpace.SoundManager.Instance.Stop();
            Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[5].Name);
            AdjustUI();
            StartCoroutine(PlayerManager.Instance.Async_Operation(lvl,lvl));
            IsLevelLoaded = true;
            PlayerManager.Instance.ReviveCount = 0;
        }
    }

    //Adjusts UI
    void AdjustUI()
    {
        PlayerManager.Instance.Timetaken = 0;
        Hide_AllLevels();
        Pause.Instance.pauseButton.SetActive(true);
        Pause.Instance.SoulsPanel.SetActive(true);
    }
  
    //takes back to chapters selection
    public void Back(GameObject chapterPanel)
    {
        Khadga.FreakSpace.SoundManager.Instance.PlaySound(Khadga.FreakSpace.SoundManager.Instance.GameSounds[5].Name);
        chapterPanel.SetActive(false);
        Menu_Text.SetActive(true);
    }

    //checks which chapter is active
    void Chapter_Status(string str,GameObject ch,GameObject ch_ul)
    {
        if (SceneManager.GetSceneByName(str) == SceneManager.GetActiveScene())
            ch.SetActive(true);
        else
            ch_ul.SetActive(true);
    }
}
