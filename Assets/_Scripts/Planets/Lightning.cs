﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Lightning : MonoBehaviour
{

    Animator Animator;
    int RandomNumber;
    public int RandomIntervalStartIndex=3;
    public int RandomIntervalEndIndex=7;

    // Use this for initialization
    void Start()
    {

        Invoke("RandomLightning", 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        Animator = GetComponent<Animator>();
    }

    public void RandomLightning()
    {
        RandomNumber = Random.Range(0, 10);
        if (RandomNumber < 5)
            Animator.SetTrigger("One");
        else
            Animator.SetTrigger("Two");

        Invoke("RandomLightning", Random.Range(RandomIntervalStartIndex, RandomIntervalEndIndex));
    }

}
