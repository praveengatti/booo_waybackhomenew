﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPortal : MonoBehaviour
{
    public GameObject Portal;
    public GameObject Exit;
    // Use this for initialization
    void Start()
    {
        Portal.SetActive(false);
        Exit.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Portal.SetActive(true);
            FindObjectOfType<Onboarding>().collectiblePopUp.SetActive(false);
            Exit.SetActive(true);
        }
    }
}
