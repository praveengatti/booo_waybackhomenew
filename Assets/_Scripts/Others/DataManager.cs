﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.UtilScripts.LocalData;
using Khadga.Localization;

public class DataManager : MonoBehaviour {

    public GameData gameData;
    public static DataManager instance;
    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else if (instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start ()
    {
        gameData = LocalDataUtil.GetLocalDataOfType<GameData>("GameData");
        LocalizationManager.ActiveLanguage = DataManager.instance.gameData.ActiveLanguage;
    }
	
	public void SaveData()
    {
        LocalDataUtil.SaveToLocal<GameData>(gameData, "GameData");
    }
}
